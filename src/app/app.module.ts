import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { JubileeLiveSavingsComponent } from './components/jubilee-live-savings/jubilee-live-savings.component';
import { AuthComponent } from './components/auth/auth.component';
import { LayoutComponent } from './components/layout/layout.component';
import { NZ_DATE_LOCALE, NZ_I18N, ru_RU } from 'ng-zorro-antd/i18n';
import { AuthenticationService } from './services/authentication.service';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { IconDefinition } from '@ant-design/icons-angular';
import {
  UserOutline,
  LockOutline,
  SendOutline,
  DeleteOutline,
  ArrowLeftOutline,
  ShareAltOutline,
  EyeOutline,
  DownloadOutline,
  CloseOutline,
  PrinterOutline,
} from '@ant-design/icons-angular/icons';
import { registerLocaleData } from '@angular/common';
import ru from '@angular/common/locales/ru';
const icons: IconDefinition[] = [
  UserOutline,
  LockOutline,
  SendOutline,
  DeleteOutline,
  ArrowLeftOutline,
  ShareAltOutline,
  EyeOutline,
  DownloadOutline,
  CloseOutline,
  PrinterOutline,
];
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AuthGuard } from './guard/AuthGuard';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { PlanService } from './services/plan.service';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { DrawerPlansComponent } from './components/drawer-plans/drawer-plans.component';
import { SavingPlansComponent } from './components/documents/saving-plans/saving-plans.component';
import { OptimumPlansComponent } from './components/documents/optimum-plans/optimum-plans.component';
import { OptimumPlusPlansComponent } from './components/documents/optimum-plus-plans/optimum-plus-plans.component';
import { InputNumberDirective } from './directives/input-numbers.directive';
import { EffectiveComponent } from './components/documents/effective/effective.component';
import { MeerimComponent } from './components/documents/meerim/meerim.component';
import { KelechekComponent } from './components/documents/kelechek/kelechek.component';
import { AngularFireRemoteConfigModule } from '@angular/fire/remote-config';
import { MetrikaModule } from 'ng-yandex-metrika';
import { ru as ruLocale } from 'date-fns/locale'

registerLocaleData(ru);

export function authenticationServiceFactory(
  authenticationService: AuthenticationService
) {
  return authenticationService;
}

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    JubileeLiveSavingsComponent,
    LayoutComponent,
    DrawerPlansComponent,
    SavingPlansComponent,
    OptimumPlansComponent,
    OptimumPlusPlansComponent,
    EffectiveComponent,
    MeerimComponent,
    KelechekComponent,
    InputNumberDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NzLayoutModule,
    NzPageHeaderModule,
    NzMenuModule,
    NzTypographyModule,
    NzNotificationModule,
    NzIconModule.forRoot(icons),
    NzGridModule,
    NzLayoutModule,
    NzSpinModule,
    NzCardModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzTypographyModule,
    NzSelectModule,
    NzBreadCrumbModule,
    NzDatePickerModule,
    NzInputNumberModule,
    NzDividerModule,
    NzSwitchModule,
    AngularFireModule.initializeApp(environment.firabese),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    NzMessageModule,
    NzPageHeaderModule,
    NzMessageModule,
    NzTableModule,
    NzAffixModule,
    NzStatisticModule,
    NzRadioModule,
    NzDrawerModule,
    AngularFireRemoteConfigModule,
    NzAlertModule,
    MetrikaModule.forRoot({
      id: 74453584,
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true,
      webvisor: true,
      triggerEvent: true
    }),
  ],
  providers: [
    AuthenticationService,
    AuthGuard,
    PlanService,
    InputNumberDirective,
    { provide: NZ_I18N, useValue: ru_RU },
    { provide: NZ_DATE_LOCALE, useValue: ruLocale }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
