import { ValuesEnum, PlanEnum } from '../const';

export class Plan {
  id?: string;
  gender: ValuesEnum;
  plan: PlanEnum;
  age: number;
  term: number;
  coefficient: number | null;
}
