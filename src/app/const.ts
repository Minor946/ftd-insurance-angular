import { formatDate } from '@angular/common';
import { FormGroup } from '@angular/forms';

export interface User {
  uid: string;
  email: string;
  displayName?: string | null;
  emailVerified: boolean;
}

export function validateAllFormFields(form: FormGroup) {
  // tslint:disable-next-line:forin
  for (const i in form.controls) {
    form.controls[i].markAsDirty();
    form.controls[i].updateValueAndValidity();
  }
}

export function calculateAge(birthday) {
  const todayFormatt = formatDate(Date.now(), 'yyyy-MM-dd', 'ru-RU');
  const birthdayFormatt = formatDate(birthday, 'yyyy-MM-dd', 'ru-RU');
  const ageDifMs =
    new Date(todayFormatt).getTime() - new Date(birthdayFormatt).getTime();
  const oneDay = 1000 * 60 * 60 * 24;
  const diff = ageDifMs / oneDay / 365.25;
  return Math.round(diff);
}

export enum ValuesEnum {
  female = 'female',
  male = 'male',
  years = 'years',
  halfYear = 'halfYear',
  quarterly = 'quarterly',
  monthly = 'monthly',
}
export enum PlanEnum {
  SavingsPlan = 'SavingsPlan',
  OptimumPlan = 'OptimumPlan',
  OptimumPlusPlan = 'OptimumPlusPlan',
  Kelechek = 'KelechekPlan',
  Effective = 'EffectivePlan',
  Meerim = 'MeerimPlan',
}

export enum MedicalEnum {
  List = 'Опросный лист',
  A = 'A*',
  AB = 'A+B*',
  ABD = 'A+B+D*',
  ACD = 'A+C+D*',
}

export interface MedicalTypes {
  type: MedicalEnum;
  yearMin: number;
  yearMax: number;
  minimal?: number;
  maximal?: number;
}
export interface SelectValue {
  label: string;
  value: ValuesEnum | PlanEnum | number;
  coefficient?: number;
  aliveCoefficient?: number;
}

export const genders: SelectValue[] = [
  {
    value: ValuesEnum.female,
    label: 'Женский',
  },
  {
    value: ValuesEnum.male,
    label: 'Мужской',
  },
];

export const plans: SelectValue[] = [
  {
    value: PlanEnum.Effective,
    label: 'Effective',
  },
  {
    value: PlanEnum.Kelechek,
    label: 'Kelechek',
  },
  {
    value: PlanEnum.SavingsPlan,
    label: 'SavingsPlan',
  },
  {
    value: PlanEnum.OptimumPlan,
    label: 'OptimumPlan',
  },
  {
    value: PlanEnum.OptimumPlusPlan,
    label: 'OptimumPlusPlan',
  },
  {
    value: PlanEnum.Meerim,
    label: 'MeerimPlan',
  },
];

export const periods: SelectValue[] = [
  {
    value: ValuesEnum.years,
    label: 'ежегодно',
    coefficient: 1,
    aliveCoefficient: 1,
  },
  {
    value: ValuesEnum.halfYear,
    label: 'полугодично',
    coefficient: 0.53,
    aliveCoefficient: 2,
  },
  {
    value: ValuesEnum.quarterly,
    label: 'ежеквартально',
    coefficient: 0.27,
    aliveCoefficient: 4,
  },
  {
    value: ValuesEnum.monthly,
    label: 'ежемесячно',
    coefficient: 0.09,
    aliveCoefficient: 12,
  },
];

export const policeTimeBonus = [
  {
    time: 10,
    value: 1.07,
  },
  {
    time: 11,
    value: 1.077,
  },

  {
    time: 12,
    value: 1.084,
  },
  {
    time: 13,
    value: 1.091,
  },
  {
    time: 14,
    value: 1.098,
  },
  {
    time: 15,
    value: 1.105,
  },
  {
    time: 16,
    value: 1.112,
  },
  {
    time: 17,
    value: 1.119,
  },
  {
    time: 18,
    value: 1.126,
  },
  {
    time: 19,
    value: 1.133,
  },
  {
    time: 20,
    value: 1.14,
  },
];

export const factors: SelectValue[] = [
  {
    label: '0 раза',
    value: 0,
  },
  {
    label: '2 раза',
    value: 2,
  },
  {
    label: '2.5 раза',
    value: 2.5,
  },
  {
    label: '3 раза',
    value: 3,
  },
];

export const medicalTypesList: MedicalTypes[] = [
  {
    type: MedicalEnum.List,
    yearMin: 18,
    yearMax: 45,
    minimal: 0,
    maximal: 25000,
  },
  {
    type: MedicalEnum.List,
    yearMin: 46,
    yearMax: 55,
    minimal: 0,
    maximal: 10000,
  },
  {
    type: MedicalEnum.List,
    yearMin: 56,
    yearMax: 60,
    minimal: 0,
    maximal: 5000,
  },

  {
    type: MedicalEnum.A,
    yearMin: 18,
    yearMax: 45,
    minimal: 25001,
    maximal: 30000,
  },
  {
    type: MedicalEnum.A,
    yearMin: 46,
    yearMax: 55,
    minimal: 10001,
    maximal: 20000,
  },
  {
    type: MedicalEnum.A,
    yearMin: 56,
    yearMax: 60,
    minimal: 5001,
    maximal: 10000,
  },

  {
    type: MedicalEnum.AB,
    yearMin: 18,
    yearMax: 45,
    minimal: 30001,
    maximal: 45000,
  },
  {
    type: MedicalEnum.AB,
    yearMin: 46,
    yearMax: 55,
    minimal: 20001,
    maximal: 30000,
  },
  {
    type: MedicalEnum.AB,
    yearMin: 56,
    yearMax: 60,
    minimal: 10001,
    maximal: 15000,
  },

  {
    type: MedicalEnum.ABD,
    yearMin: 18,
    yearMax: 45,
    minimal: 45001,
    maximal: 60000,
  },
  {
    type: MedicalEnum.ABD,
    yearMin: 46,
    yearMax: 55,
    minimal: 30001,
    maximal: 45000,
  },
  {
    type: MedicalEnum.ABD,
    yearMin: 56,
    yearMax: 60,
    minimal: 15001,
    maximal: 20000,
  },

  {
    type: MedicalEnum.ACD,
    yearMin: 18,
    yearMax: 45,
    minimal: 60001,
  },
  {
    type: MedicalEnum.ACD,
    yearMin: 46,
    yearMax: 55,
    minimal: 45001,
  },
  {
    type: MedicalEnum.ACD,
    yearMin: 56,
    yearMax: 60,
    minimal: 20001,
  },
];

export function professionCoefficients() {
  const coeffocients: SelectValue[] = [];
  let coeff = 0;
  for (let i = 0; i <= 20; i++) {
    coeffocients.push({
      label: coeff.toString(),
      value: parseFloat(coeff.toFixed(2)),
    });
    coeff += 0.5;
  }
  return coeffocients;
}

export function medicalFactors() {
  const coeffocients: SelectValue[] = [];
  coeffocients.push({
    label: 0 + '%',
    value: 0,
  });
  coeffocients.push({
    label: 50 + '%',
    value: 50,
  });
  let coeff = 50;
  for (let i = 0; i < 14; i++) {
    coeff += 25;
    coeffocients.push({
      label: coeff + '%',
      value: coeff,
    });
  }
  return coeffocients;
}
