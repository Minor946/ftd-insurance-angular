import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();

  loading = false;
  countUnreadChatMessages: Observable<number>;

  constructor(
    protected router: Router,
    private authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    if (this.router.url === '/') {
      this.loading = true;
      return;
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  isAuth() {
    return this.authService.isLoggedIn;
  }

  logout() {
    this.authService.signOut();
  }
}
