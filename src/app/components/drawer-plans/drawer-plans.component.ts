import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { PlanEnum } from '../../const';
import html2PDF from 'jspdf-html2canvas';
import { Metrika } from 'ng-yandex-metrika';

@Component({
  selector: 'app-drawer-plans',
  templateUrl: './drawer-plans.component.html',
  styleUrls: ['./drawer-plans.component.scss'],
})
export class DrawerPlansComponent implements OnInit {
  @Input() data: any;

  isLoading = false;

  constructor(private drawerRef: NzDrawerRef, private metrika: Metrika) {}

  ngOnInit(): void {
    this.metrika.fireEvent(this.data.plan, {
      title: 'Данные',
      params: this.data,
    });
  }

  isSavingPlan() {
    return this.data.plan === PlanEnum.SavingsPlan;
  }
  isOptimumPlan() {
    return this.data.plan === PlanEnum.OptimumPlan;
  }
  isOptimumPlusPlan() {
    return this.data.plan === PlanEnum.OptimumPlusPlan;
  }
  isKelechek() {
    return this.data.plan === PlanEnum.Kelechek;
  }
  isEffective() {
    return this.data.plan === PlanEnum.Effective;
  }
  isMeerim() {
    return this.data.plan === PlanEnum.Meerim;
  }

  closeDrawer() {
    this.drawerRef.close();
  }

  // generatePDF() {
  //   var element = document.getElementById('plan-table'); //Id of the table
  //   const withOffset = this.isKelechek() || this.isSavingPlan();
  //   html2canvas(element, {
  //     scrollY: 0,
  //   }).then(function (canvas) {
  //     canvas.getContext('2d');
  //     let pdf = new jsPDF('p', 'pt', [canvas.width + 15, canvas.height + 30]);
  //     const contentDataURL = canvas.toDataURL('image/png', 1.0);
  //     pdf.addImage(
  //       contentDataURL,
  //       'PNG',
  //       0,
  //       withOffset ? 0 : 10,
  //       canvas.width,
  //       canvas.height,
  //       undefined,
  //       'MEDIUM'
  //     );
  //     pdf.save();
  //   });
  // }

  generatePDF2() {
    const closeEmitter: EventEmitter<any> = new EventEmitter();
    closeEmitter.subscribe(() => {
      this.isLoading = false;
      this.closeDrawer();
    });

    this.isLoading = true;
    var element = document.getElementById('plan-table'); //Id of the table
    const withOffset = this.isKelechek() || this.isSavingPlan();
    const date = new Date();
    const fileName = this.data.plan + date.toDateString() + '.pdf';
    html2PDF(element, {
      jsPDF: {
        unit: 'px',
        format: 'a4',
      },
      html2canvas: {
        imageTimeout: 15000,
        logging: true,
        useCORS: false,
      },
      imageType: 'image/jpeg',
      imageQuality: 1,
      margin: {
        top: withOffset ? 15 : 30,
        right: 0,
        bottom: 0,
        left: 0,
      },
      output: fileName,
      init: function () {},
      success: function (pdf) {
        pdf.save(this.output);
        closeEmitter.emit();
      },
    });
  }

  printPdf() {
    const sTable = document.getElementById('plan-table').innerHTML;
    const styles = document.querySelector(
      'link[rel=stylesheet][href^=styles][href*=css]'
    );
    const stylesHref = styles.getAttribute('href');
    const win = window.open('', '', 'height=728,width=1024');
    win.document.write('<html><head>');
    win.document.write('<title>Pdf file</title>'); // <title> FOR PDF HEADER.
    win.document.write('<link rel="stylesheet" href="' + stylesHref + '">'); // <title> FOR PDF HEADER.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable); // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); // CLOSE THE CURRENT WINDOW.
  }
}
