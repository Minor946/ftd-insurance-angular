import { formatNumber } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { periods, SelectValue, policeTimeBonus } from '../../../const';

@Component({
  selector: 'app-meerim',
  templateUrl: './meerim.component.html',
})
export class MeerimComponent implements OnInit {
  @Input() data: any;
  today = new Date();
  bithdayMoment = null;
  periods = periods;
  period: SelectValue = null;
  bonuses = policeTimeBonus;

  constructor() {}

  ngOnInit(): void {
    this.bithdayMoment = this.getMount(this.data.bithday);
    this.period = this.periods.find(
      (item) => item.value === this.data.policePedriod
    );
  }

  getMount(date) {
    var objDate = date;

    var locale = 'ru-RU',
      month = objDate.toLocaleString(locale, { month: 'long' });
    return month;
  }

  getFormatNumber(number) {
    return formatNumber(parseInt(parseFloat(number).toFixed(0)), 'ru-RU');
  }

  isShowAdditianPremium() {
    return (
      this.data.medicalFactor > 0 &&
      this.data.additionalPremium > 0 &&
      this.data.professionCoefficient > 0 &&
      this.data.isDisabilityAccidentFactor > 0 &&
      this.data.isAccidentalDeathFactor > 0
    );
  }
}
