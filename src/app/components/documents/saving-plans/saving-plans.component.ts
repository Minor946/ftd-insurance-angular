import { formatNumber } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { periods, SelectValue, ValuesEnum } from '../../../const';

@Component({
  selector: 'app-saving-plans',
  templateUrl: './saving-plans.component.html',
})
export class SavingPlansComponent implements OnInit {
  @Input() data: any;
  today = new Date();
  bithdayMoment = null;
  periods = periods;
  period: SelectValue = null;
  redemptions: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  paymentByPeriod = null;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.bithdayMoment = this.getMount(this.data.bithday);
    this.period = this.periods.find(
      (item) => item.value === this.data.policePedriod
    );
    this.loadPaymentsByPeriod();
  }

  getMount(date) {
    var objDate = date;
    var locale = 'ru-RU',
      month = objDate.toLocaleString(locale, { month: 'long' });
    return month;
  }

  getFormatNumber(number) {
    if (number === '-') {
      return number;
    }
    return formatNumber(parseInt(parseFloat(number).toFixed(0)), 'ru-RU');
  }

  getRedemption(year) {
    if (parseInt(year) === this.data.policeTime) {
      return this.data.incurancePrice;
    }
    if (this.paymentByPeriod && this.paymentByPeriod[year]) {
      const paymentCoeficient = this.paymentByPeriod[year].find((item) => {
        return parseInt(item.age) === parseInt(this.data.yearDiff);
      });
      if (paymentCoeficient[this.data.policeTime] === '-') {
        return paymentCoeficient[this.data.policeTime];
      } else {
        return (
          (paymentCoeficient[this.data.policeTime] * this.data.incurancePrice) /
          1000
        ).toFixed();
      }
    }
    return '-';
  }

  isShowAdditianPremium() {
    return (
      this.data.medicalFactor > 0 &&
      this.data.additionalPremium > 0 &&
      this.data.professionCoefficient > 0 &&
      this.data.isDisabilityAccidentFactor > 0 &&
      this.data.isAccidentalDeathFactor > 0
    );
  }

  loadPaymentsByPeriod() {
    let file = null;
    if (this.data.gender === ValuesEnum.female) {
      file = './assets/SavingsPlanFemale.json';
    }
    if (this.data.gender === ValuesEnum.male) {
      file = './assets/SavingsPlanMale.json';
    }
    if (file) {
      this.http.get(file).subscribe((file) => {
        this.paymentByPeriod = file;
      });
    }
  }
}
