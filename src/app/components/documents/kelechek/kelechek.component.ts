import { formatNumber } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { periods, SelectValue, ValuesEnum } from '../../../const';

@Component({
  selector: 'app-kelechek',
  templateUrl: './kelechek.component.html',
})
export class KelechekComponent implements OnInit {
  @Input() data: any;
  today = new Date();
  bithdayMoment = null;
  paymentByPeriod = null;
  periods = periods;
  period: SelectValue = null;
  redemptions: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  additionalPlan: any[] = [];
  kelechekPlan: any[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.bithdayMoment = this.getMount(this.data.bithday);
    this.period = this.periods.find(
      (item) => item.value === this.data.policePedriod
    );
    this.loadPaymentsByPeriod();
  }

  getMount(date) {
    var objDate = date;
    var locale = 'ru-RU',
      month = objDate.toLocaleString(locale, { month: 'long' });
    return month;
  }

  getFormatNumber(number) {
    if (number === '-') {
      return number;
    }
    return formatNumber(parseInt(parseFloat(number).toFixed(0)), 'ru-RU');
  }

  getRedemption(year) {
    if (parseInt(year) === this.data.policeTime + 1) {
      return formatNumber(
        this.data.incurancePrice.toFixed(0),
        'ru-RU'
      );
    }
    if (year > 2) {
      const redemptionValue = this.kelechekPlan.find((item) => {
        return item.month == (year - 1) * 12 + 1;
      });
      if (redemptionValue?.surrenderBenefitSum) {
        return formatNumber(
          redemptionValue.surrenderBenefitSum.toFixed(0),
          'ru-RU'
        );
      }
    }
    return '-';
  }

  isShowAdditianPremium() {
    return (
      this.data.medicalFactor > 0 &&
      this.data.additionalPremium > 0 &&
      this.data.professionCoefficient > 0 &&
      this.data.isDisabilityAccidentFactor > 0 &&
      this.data.isAccidentalDeathFactor > 0
    );
  }

  updateAdditionalPlan() {
    this.kelechekPlan = [];
    let countMonth = 1;
    let npx = 1;
    let vn = 1;
    for (let i = 0; i < this.data.policeTime; i++) {
      for (let j = 1; j <= 12; j++) {
        const additionCoefficient = this.additionalPlan.find((item) => {
          return item.age == this.data.yearDiff + i;
        });
        const qxMortality =
          this.data.gender == ValuesEnum.male
            ? additionCoefficient.surrenderBenefitMale
            : additionCoefficient.surrenderBenefitFemale;

        let px = 1 - qxMortality;
        let prevNpx = npx;
        npx = npx * px;
        const n1pxVn1 = prevNpx * vn;
        vn = Math.pow(1.00486755056534, countMonth * -1);
        let isPremium = false;
        switch (this.data.policePedriod) {
          case ValuesEnum.monthly:
            isPremium = true;
            break;
          case ValuesEnum.quarterly:
            isPremium = j == 1 || j == 4 || j == 7 || j == 10 ? true : false;
            break;
          case ValuesEnum.halfYear:
            isPremium = j == 1 || j == 7 ? true : false;
            break;
          case ValuesEnum.years:
            isPremium = j == 1 ? true : false;
            break;
        }
        this.kelechekPlan.push({
          year: i,
          month: countMonth,
          age: this.data.yearDiff + i,
          premium: isPremium,
          qxMortality,
          px,
          npx,
          vn,
          vncountMonth: countMonth,
          n1pxQxn1vn: qxMortality * prevNpx * vn,
          n1pxVn1,
          pvPremium: isPremium ? n1pxVn1 : 0,
        });
        countMonth = countMonth + 1;
      }
    }
    npx = 1;
    vn = 1;

    this.kelechekPlan.map((item, index) => {
      const vpPremiumSum = this.kelechekPlan.reduce(
        (previousValue, currentValue, currentIndex) => {
          return currentIndex >= index
            ? previousValue + currentValue.pvPremium
            : previousValue;
        },
        0
      );
      item['axn'] = vpPremiumSum / (npx * vn);
      item['deathMaturityBenefitPV'] =
        index == this.kelechekPlan.length - 1
          ? this.data.incurancePrice * npx * item.vn
          : this.data.incurancePrice * item.n1pxQxn1vn;
      npx = item.npx;
      vn = item.vn;
    });
    npx = 1;
    vn = 1;

    const DeathMaturityBenefitPVSum = this.kelechekPlan.reduce(
      (previousValue, currentValue) => {
        return previousValue + currentValue.deathMaturityBenefitPV;
      },
      0
    );

    const netPremium = DeathMaturityBenefitPVSum / this.kelechekPlan[0].axn;
    this.kelechekPlan.map((item, index) => {
      const itemDeathMaturityBenefitPVSum = this.kelechekPlan.reduce(
        (previousValue, currentValue, currentIndex) => {
          return currentIndex >= index
            ? previousValue + currentValue.deathMaturityBenefitPV
            : previousValue;
        },
        0
      );
      item['pVBenefitsSum'] = itemDeathMaturityBenefitPVSum / (npx * vn);
      item['pVPremiumSum'] = item.axn * netPremium;
      item['surrenderBenefitSum'] =
        item['pVBenefitsSum'] - item['pVPremiumSum'] > 0
          ? item['pVBenefitsSum'] - item['pVPremiumSum']
          : 0;
      npx = item.npx;
      vn = item.vn;
    });
  }

  loadPaymentsByPeriod() {
    this.http.get('./assets/KelechekPlanNew.json').subscribe((file) => {
      file['kelecheck'].map((item) => {
        this.additionalPlan.push({
          age: item['age'],
          male: item['male'],
          female: item['female'],
          surrenderBenefitMale: ((item['male'] / 1000) * 1) / 12,
          surrenderBenefitFemale: ((item['female'] / 1000) * 1) / 12,
        });
      });
      this.updateAdditionalPlan();
    });
  }
}
