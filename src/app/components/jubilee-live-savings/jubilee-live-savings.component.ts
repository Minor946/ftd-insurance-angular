import { formatNumber } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  MaxLengthValidator,
  Validators,
} from '@angular/forms';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import {
  calculateAge,
  genders,
  periods,
  ValuesEnum,
  SelectValue,
  factors,
  professionCoefficients,
  medicalFactors,
  plans,
  PlanEnum,
  validateAllFormFields,
  medicalTypesList,
  policeTimeBonus,
} from '../../const';
import { Plan } from '../../models/plan.model';
import { DrawerPlansComponent } from '../drawer-plans/drawer-plans.component';
import { AngularFireRemoteConfig } from '@angular/fire/remote-config';

@Component({
  selector: 'app-jubilee-live-savings',
  templateUrl: './jubilee-live-savings.component.html',
})
export class JubileeLiveSavingsComponent implements OnInit {
  form: FormGroup;
  planListNew: any[] = [];
  firebaseplans: any[] = [];
  yearDiff: number = null;
  genders = genders;
  periods = periods;
  factors = factors;
  medicalTypesList = medicalTypesList;
  plans = plans;
  professionCoefficients = professionCoefficients();
  medicalFactors = medicalFactors();
  today = new Date();
  basicPremium = 0;
  aliveSum = 0;
  additionalPremium = 0;
  usd = null;
  user: any = null;
  bonuses = policeTimeBonus;
  incurancePriceLimit: number;
  minPoliceTimeLimit: number = 10;
  maxPoliceTimeLimit: number = 20;

  selectedPeriod: SelectValue = null;
  lastPlan = null;
  checkIsAccidentalDeathFactor = false;
  checkIsDisabilityAccidentFactor = false;
  dateFormat = 'dd.MM.yyyy';

  formatterPrice = (value: number) => this.getFormatNumber(value);
  parsePrice = (value: string) => parseInt(value.replace(/\s+/g, ''));

  constructor(
    private fb: FormBuilder,
    private drawerService: NzDrawerService,
    private remoteConfig: AngularFireRemoteConfig,
    private http: HttpClient
  ) {
    this.user = JSON.parse(sessionStorage.getItem('user'));
    this.loadPlans();
    var date = new Date();
    date.setFullYear(date.getFullYear() - 19);
    this.form = this.fb.group({
      plan: [PlanEnum.Effective, [Validators.required]],
      fullName: [null, [Validators.required]],
      gender: [ValuesEnum.female, [Validators.required]],
      bithday: [date, [Validators.required]],
      incurancePrice: [2000000, [Validators.required]],
      policeTime: [10, [Validators.required]],
      policePedriod: [ValuesEnum.quarterly, [Validators.required]],
      agent: [this.user?.displayName],
      numberDocument: [null],
      isAccidentalDeath: [true],
      isAccidentalDeathPremium: [null],
      isAccidentalDeathAddPremium: [null],
      isAccidentalDeathSumInsured: [1000000],
      isAccidentalDeathFactor: [2.5],
      isDisabilityAccident: [true],
      isDisabilityAccidentPremium: [null],
      isDisabilityAccidentAddPremium: [null],
      isDisabilityAccidentSumInsured: [1000000],
      isDisabilityAccidentFactor: [2],
      professionCoefficient: [1],
      medicalFactor: [50],
    });
    this.yearDiff = calculateAge(date);
    this.calculate(this.form.value);
    this.form.valueChanges.subscribe((values) => {
      if (values.bithday) {
        this.yearDiff = calculateAge(values.bithday);
      } else {
        this.yearDiff = null;
      }
      this.calculate(values);
      this.lastPlan = values.plan;
      
    });

    this.form.controls['plan'].valueChanges.subscribe((value) => {
      if (value === PlanEnum.Meerim) {
        this.minPoliceTimeLimit = 3;
        this.maxPoliceTimeLimit = 10;

        this.form.controls['policeTime'].setValue(10);
        this.form.controls['policePedriod'].setValue(ValuesEnum.years);
        this.form.controls['incurancePrice'].setValue(200000);
        this.form.controls['isAccidentalDeathSumInsured'].setValue(200000);
        this.form.controls['isDisabilityAccidentSumInsured'].setValue(200000);

        this.periods = this.periods.filter((item) => {
          return item.value !== ValuesEnum.quarterly && item.value !== ValuesEnum.monthly;
        })
      } else {
        this.minPoliceTimeLimit = 10;
        this.maxPoliceTimeLimit = 20;

        this.form.controls['policeTime'].setValue(10);

        this.periods = periods;
      }
    })
  }

  ngOnInit(): void {
    this.remoteConfig.fetchAndActivate().then(async () => {
      this.usd = await this.remoteConfig.getNumber('usd_currency');
      const planString = await this.remoteConfig.getString('plans');
      const planJson = JSON.parse(planString);
      this.plans = this.plans.filter((item) => {
        return planJson.plans[0][item.value.toString()] === 'true';
      })
    });
  }

  isSavingPlan() {
    return this.form?.value?.plan === PlanEnum.SavingsPlan;
  }
  isKelechek() {
    return this.form?.value?.plan === PlanEnum.Kelechek;
  }

  isOptimumPlusPlan() {
    return this.form?.value?.plan === PlanEnum.OptimumPlusPlan;
  }

  isMeerimPlan() {
    return this.form?.value?.plan === PlanEnum.Meerim;
  }

  calculate(values) {
    if (!this.checkPreview()) {
      return false;
    }
    this.selectedPeriod = this.periods.find((item) => {
      return item.value === values.policePedriod;
    });
    const planPrice =
      (values.incurancePrice *
        parseFloat(
          this.planCoefficient(
            values.plan,
            values.gender,
            values.policeTime
          ).toFixed(2)
        )) /
      1000;
    this.basicPremium = planPrice * this.selectedPeriod.coefficient;

    if (this.isMeerimPlan()) {
      const premiumCalaulation = (
        (
          parseFloat(
            this.planCoefficient(
              values.plan,
              values.gender,
              values.policeTime
            ).toFixed(2)
          ) * values.incurancePrice
        )) / 1000;
  
      this.additionalPremium = parseFloat(
        (
          (
            (values.medicalFactor * premiumCalaulation / 100) +
            (values.incurancePrice * 0.0015)
          ) * this.selectedPeriod.coefficient
        ).toFixed(2)
      )
    } else {
      this.additionalPremium =
      parseInt(
        (
          (values.professionCoefficient / 1000) * values.incurancePrice +
          (planPrice * values.medicalFactor) / 100
        ).toFixed()
      ) * this.selectedPeriod.coefficient;
    }

    this.getAliveSum();
    if (values.isAccidentalDeath) {
      const isAccidentalDeathPremium =
        values.isAccidentalDeathSumInsured * 0.002;
      const isAccidentalDeathAddPremium =
        values.isAccidentalDeathFactor > 0
          ? isAccidentalDeathPremium * values.isAccidentalDeathFactor
          : isAccidentalDeathPremium;
      this.form.patchValue(
        {
          isAccidentalDeathPremium:
            isAccidentalDeathPremium * this.selectedPeriod.coefficient,
          isAccidentalDeathAddPremium:
            (isAccidentalDeathAddPremium - isAccidentalDeathPremium) *
            this.selectedPeriod.coefficient,
        },
        {
          emitEvent: false,
          onlySelf: true,
        }
      );
    }

    if (values.isDisabilityAccident) {
      const isDisabilityAccidentPremium =
        values.isDisabilityAccidentSumInsured * 0.001;
      const isDisabilityAccidentAddPremium =
        values.isDisabilityAccidentFactor > 0
          ? isDisabilityAccidentPremium * values.isDisabilityAccidentFactor
          : isDisabilityAccidentPremium;

      this.form.patchValue(
        {
          isDisabilityAccidentPremium:
            isDisabilityAccidentPremium * this.selectedPeriod.coefficient,
          isDisabilityAccidentAddPremium:
            (isDisabilityAccidentAddPremium - isDisabilityAccidentPremium) *
            this.selectedPeriod.coefficient,
        },
        {
          emitEvent: false,
          onlySelf: true,
        }
      );
    }
  }

  totalSumPremium() {
    let sum = this.basicPremium + this.additionalPremium;
    if (this.form.get('isDisabilityAccident').value) {
      sum += this.form.get('isDisabilityAccidentPremium').value
        ? parseFloat(this.form.get('isDisabilityAccidentPremium').value)
        : 0;
      sum += this.form.get('isDisabilityAccidentAddPremium').value
        ? parseFloat(this.form.get('isDisabilityAccidentAddPremium').value)
        : 0;
    }
    if (this.form.get('isAccidentalDeath').value) {
      sum += this.form.get('isAccidentalDeathPremium').value
        ? parseFloat(this.form.get('isAccidentalDeathPremium').value)
        : 0;
      sum += this.form.get('isAccidentalDeathAddPremium').value
        ? parseFloat(this.form.get('isAccidentalDeathAddPremium').value)
        : 0;
    }
    return sum;
  }

  planCoefficient(plan, gender, policeTime) {
    let coeficient = 0;
    this.planListNew.find((item) => {
      if (item.plan === plan) {
        item[gender].find((value) => {
          if (parseInt(value.age) === this.yearDiff) {
            coeficient = value[policeTime];
          }
        });
      }
    });
    return parseFloat(coeficient ? coeficient.toString() : '0');
  }

  async loadPlans() {
    await plans.map((item) => {
      this.http
        .get('./assets/' + item.value.toString() + '.json')
        .subscribe((file) => {
          this.planListNew.push({
            plan: item.value,
            male: file['male'],
            female: file['female'],
          });
          this.calculate(this.form.value);
        });
    });
  }

  getAliveSum() {
    let bonus = null;
    if (
      this.form?.value?.plan === PlanEnum.OptimumPlusPlan ||
      this.form?.value?.plan === PlanEnum.Effective
    ) {
      bonus = this.bonuses.find(
        (item) => item.time === this.form.value.policeTime
      );
    }

    this.aliveSum =
      (this.basicPremium + this.additionalPremium) *
      this.form.value.policeTime *
      this.selectedPeriod.aliveCoefficient *
      (bonus ? bonus.value : 1);
  }

  preview() {
    validateAllFormFields(this.form);
    if (this.form.valid) {
      const data = {
        ...this.form.value,
        additionalPremium: this.additionalPremium,
        basicPremium: this.basicPremium,
        yearDiff: this.yearDiff,
        totalSumPremium: this.totalSumPremium(),
        medicatalType: this.getMedicatalType(),
        isToAlive: this.aliveSum,
      };
      this.drawerService.create({
        nzTitle: 'Превью документа',
        nzContent: DrawerPlansComponent,
        nzWidth: '100%',
        nzContentParams: {
          data,
        },
      });
    }
  }

  getFormatNumber(number) {
    return formatNumber(Math.round(parseFloat(number)), 'ru-RU');
  }

  getMedicatalType() {
    const price = this.form.get('incurancePrice').value;

    const medicataType = medicalTypesList.find((item) => {
      let selected = null;
      if (this.yearDiff >= item.yearMin && this.yearDiff <= item.yearMax) {
        if (item.maximal) {
          selected =
            price >= item.minimal * this.usd && price <= item.maximal * this.usd
              ? item
              : null;
        } else {
          selected = price >= item.minimal * this.usd ? item : null;
        }
      }

      return selected ?? selected;
    });
    return medicataType ? medicataType.type : '';
  }

  getMaximalInsured() {
    if (this.form.value.incurancePrice < 200000) {
      return Math.min(this.form.value.incurancePrice * 2, 200000);
    } else {
      return this.form.value.incurancePrice;
    }
  }

  checkIsAccidentalDeathSumInsured() {
    const minimal = this.getMaximalInsured();
    if (!this.form.value.isAccidentalDeath) {
      return true;
    }
    if (
      this.form.value.incurancePrice < 200000 &&
      this.form.value.isAccidentalDeathSumInsured <= minimal
    ) {
      return true;
    }
    if (
      this.form.value.isAccidentalDeathSumInsured <= minimal &&
      this.form.value.incurancePrice >= 200000
    ) {
      return true;
    }
    return false;
  }

  checkIsDisabilityAccidentSumInsured() {
    const minimal = this.getMaximalInsured();
    if (!this.form.value.isDisabilityAccident) {
      return true;
    }
    if (
      this.form.value.incurancePrice < 200000 &&
      this.form.value.isDisabilityAccidentSumInsured <= minimal
    ) {
      return true;
    }
    if (
      this.form.value.isDisabilityAccidentSumInsured <= minimal &&
      this.form.value.incurancePrice >= 200000
    ) {
      return true;
    }
    return false;
  }

  checkIsIncurancePrice() {
    if (this.isMeerimPlan()) {
      this.incurancePriceLimit = 1500000;
      return this.form.value.incurancePrice <= 1500000;
    }
    this.incurancePriceLimit = 200000000;
    return this.form.value.incurancePrice <= 200000000;
  }

  checkIsYearDiff() {
    return this.yearDiff >= 18 && this.yearDiff <= 55;
  }

  checkIsPolicePeriod() {
    const ages = this.yearDiff + this.form.value.policeTime;
    return ages <= 65;
  }

  onChangeIsAccidentalDeath(value: boolean): void {
    if (!value) {
      this.form.patchValue({
        isAccidentalDeathFactor: 0,
      });
      this.checkIsAccidentalDeathFactor = true;
    } else {
      this.checkIsAccidentalDeathFactor = false;
    }
  }

  onChangeIsDisabilityAccident(value: boolean): void {
    if (!value) {
      this.form.patchValue({
        isDisabilityAccidentFactor: 0,
      });
      this.checkIsDisabilityAccidentFactor = true;
    } else {
      this.checkIsDisabilityAccidentFactor = false;
    }
  }

  checkPreview() {
    return (
      this.checkIsAccidentalDeathSumInsured() &&
      this.checkIsIncurancePrice() &&
      this.checkIsYearDiff() &&
      this.checkIsDisabilityAccidentSumInsured()
    );
  }
}
