import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  form: FormGroup;
  loading = false;
  showConfirmEmail = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private messageService: NzMessageService
  ) {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });
    this.showConfirmEmail = this.authService.isNeedConfirm;
    if (this.authService.isLoggedIn) {
      this.router.navigateByUrl('/dashboard');
    }
  }

  public ngOnInit(): void {}

  submitForm() {
    this.loading = true;
    if (this.form.valid) {
      this.authService
        .signIn(this.form.get('email').value, this.form.get('password').value)
        .then((result) => {
          this.router.navigateByUrl('/dashboard');
        })
        .catch((error) => {
          this.messageService.error(error.message);
        });
    } else {
      this.loading = false;
    }
  }

  sendConfirmEmail() {
    if (this.authService.isNeedConfirm) {
      this.authService.sendConfirmEmail();
    }
  }
}
