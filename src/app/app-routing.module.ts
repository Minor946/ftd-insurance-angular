import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { JubileeLiveSavingsComponent } from './components/jubilee-live-savings/jubilee-live-savings.component';
import { LayoutComponent } from './components/layout/layout.component';
import { AuthGuard } from './guard/AuthGuard';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'sign-in', component: AuthComponent },
      {
        path: 'dashboard',
        component: JubileeLiveSavingsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: '**',
        redirectTo: '/dashboard',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
