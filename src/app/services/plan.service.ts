import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plan } from '../models/plan.model';

@Injectable({
  providedIn: 'root',
})
export class PlanService {
  constructor(private firestore: AngularFirestore) {}

  getPlans() {
    return this.firestore.collection('plans').snapshotChanges();
  }

  createPlan(plan: Plan) {
    sessionStorage.removeItem('plans');
    return this.firestore.collection('plans').add(plan);
  }

  updatePlan(plan: Plan) {
    delete plan.id;
    this.firestore.doc('plans/' + plan.id).update(plan);
    sessionStorage.removeItem('plans');
  }

  deletePolicy(policyId: string) {
    sessionStorage.removeItem('plans');
    this.firestore.doc('plans/' + policyId).delete();
  }
}
