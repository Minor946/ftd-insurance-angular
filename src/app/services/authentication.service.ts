import { Injectable, NgZone } from '@angular/core';
import { User } from '../const';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Metrika } from 'ng-yandex-metrika';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  userData: User;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone,
    private messageService: NzMessageService,
    private metrika: Metrika
  ) {
    this.afAuth.authState.subscribe((user) => {
      const userRef: AngularFirestoreDocument<any> = this.afs.doc(
        `users/${user.uid}`
      );
      if (user) {
        this.userData = {
          displayName: user.displayName,
          email: user.email,
          emailVerified: user.emailVerified,
          uid: user.uid,
        };
        sessionStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(sessionStorage.getItem('user'));
      } else {
        sessionStorage.setItem('user', null);
        JSON.parse(sessionStorage.getItem('user'));
      }
    });
  }

  signIn(email, password) {
    return this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.setUserData(result.user);
        window.location.href = '/dashboard';
      })
      .catch((error) => {
        this.messageService.error(error.message);
      });
  }

  async sendConfirmEmail() {
    (await this.afAuth.currentUser).sendEmailVerification().then(() => {
      this.messageService.success('Письмо отправлено');
    });
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(sessionStorage.getItem('user'));
    return user !== null && user.emailVerified !== false ? true : false;
  }

  get isNeedConfirm(): boolean {
    const user = JSON.parse(sessionStorage.getItem('user'));
    return user !== null && user.emailVerified === false ? true : false;
  }

  setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.uid}`
    );
    const userData: User = {
      uid: user.uid,
      email: user.email,
      emailVerified: user.emailVerified,
    };

    this.metrika.setUserID(user.uid);
    this.metrika.userParams(userData);
    return userRef.set(userData, {
      merge: true,
    });
  }

  signOut() {
    return this.afAuth.signOut().then(() => {
      sessionStorage.removeItem('user');
      this.router.navigate(['sign-in']);
    });
  }
}
