// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firabese: {
    apiKey: 'AIzaSyCOFYHob94niSENojqIzbshJoKxi6-yDJI',
    authDomain: 'insurance-d34eb.firebaseapp.com',
    databaseURL: 'https://insurance-d34eb-default-rtdb.firebaseio.com',
    projectId: 'insurance-d34eb',
    storageBucket: 'insurance-d34eb.appspot.com',
    messagingSenderId: '1046172311653',
    appId: '1:1046172311653:web:d64fa1ec7026df36c0268c',
    measurementId: 'G-5Y5NPNBS4T',
  },
};
